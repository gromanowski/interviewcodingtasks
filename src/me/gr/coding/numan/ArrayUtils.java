package me.gr.coding.numan;

import java.util.Arrays;

public class ArrayUtils {

    // Write a program to display numbers having sum of left side numbers equal to right side numbers
    // E.g. {1,0,-11,1,12}=>0 {Left side number 1+0=1, Right side number -11+1+12=1}.

    /**
     * Print two groups of numbers having the same value of their sums.
     *
     * @param
     * @return right index of the left sub-array or -1 if there is no such groups
     */
    public static int printEqualValueSidesNumbersN2(int[] numbers) {
        if (numbers == null || numbers.length == 0) {
            System.err.println(numbers == null ? "input array is null" : "input array is empty");
            return -1;
        }

        System.out.println(Arrays.toString(numbers));

        int leftSubArrayRightIndex = -1;

        for (int i = 0; i < numbers.length - 1; i++) {
            final int leftSum = getSum(numbers, 0, i + 1);
            final int rightSum = getSum(numbers, i + 1, numbers.length);
            System.out.println("left sum is " + leftSum + ", right sum is " + rightSum);

            if (leftSum == rightSum) {
                leftSubArrayRightIndex = i;
            } else if (leftSubArrayRightIndex != -1) {
                break;
            }
        }

        if (leftSubArrayRightIndex == -1) {
            System.out.println("there is no such a sub-sequence in the array which sum is equal to the one of rest of the array");
        } else {
            System.out.println(Arrays.toString(Arrays.copyOfRange(numbers, 0, leftSubArrayRightIndex + 1)));
            System.out.println(Arrays.toString(Arrays.copyOfRange(numbers, leftSubArrayRightIndex + 1, numbers.length)));
            System.out.println();
        }

        return leftSubArrayRightIndex;
    }

    /**
     * Sums numbers in the passed array within provided bounds
     *
     * @param numbers    array of numbers
     * @param leftBound  left bound index inclusive
     * @param rightBound right bound index exclusive
     * @return sum
     */
    private static int getSum(int[] numbers, int leftBound, int rightBound) {
        int sum = 0;

        for (int j = leftBound; j < rightBound; j++) {
            sum += numbers[j];
        }

        return sum;
    }

    /**
     * Print two groups of numbers having the same value of their sums.
     *
     * @param
     * @return right index of the left sub-array or -1 if there is no such groups
     */
    public static int printEqualValueSidesNumbers2N(int[] numbers) {
        if (numbers == null || numbers.length == 0) {
            System.err.println(numbers == null ? "input array is null" : "input array is empty");
            return -1;
        }

        System.out.println(Arrays.toString(numbers));

        final int total = getTotalSum(numbers);
        int leftSubArrayRightIndex = -1;
        int leftSum = 0;
        int rightSum = total;

        for (int i = 0; i < numbers.length - 1; i++) {
            leftSum += numbers[i];
            rightSum -= numbers[i];
            System.out.println("left sum is " + leftSum + ", right sum is " + rightSum);

            if (leftSum == rightSum) {
                leftSubArrayRightIndex = i;
            } else if (leftSubArrayRightIndex != -1) {
                break;
            }
        }


        return leftSubArrayRightIndex;
    }

    private static int getTotalSum(int[] numbers) {
        int totalSum = 0;

        for (int i : numbers) {
            totalSum += i;
        }

        return totalSum;
    }
}
