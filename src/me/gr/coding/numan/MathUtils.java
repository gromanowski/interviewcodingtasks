package me.gr.coding.numan;

public class MathUtils {

    // Write a function to calculate x in the power of y.
    // Assuming that it's enough to provide solution for integer y.

    /**
     * Method to calculate integer x in power of integer y.
     * O(n) where n = y
     *
     * @param x float number to be powered to y
     * @param y integer number to power x
     * @return x in power y
     */
    public static float power(float x, int y) {
        if (y == 0) {
            return 1;
        }

        float multiplier = y > 0 ? x : 1.0f / x;

        float c = 1.f;

        for (int i = y > 0 ? y : -y; i > 0; i--) {
            c *= multiplier;
        }

        return c;
    }

    /**
     * Method to calculate float x in the power of integer y.
     *
     * @param x float number to be powered to y
     * @param y integer number to power x
     * @return x in power y
     */
    public static float xInPowerOfY(float x, int y) {
        if (y == 0) {
            return 1;
        }

        float temp = xInPowerOfY(x, y / 2);

        if (y % 2 == 0)
            return temp * temp;
        else if (y > 0) {
            return x * temp * temp;
        } else {
            return (temp * temp) / x;
        }
    }
}
