package me.gr.coding.numan;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MathUtilsTests {

    @Test
    public void testPower() {
        assertEquals(Math.pow(2, 0), MathUtils.power(2, 0), 0.0);
        assertEquals(Math.pow(2, 1), MathUtils.power(2, 1), 0.0);
        assertEquals(Math.pow(2, 2), MathUtils.power(2, 2), 0.0);
        assertEquals(Math.pow(2, 3), MathUtils.power(2, 3), 0.0);
        assertEquals(Math.pow(2, 5), MathUtils.power(2, 5), 0.0);
        assertEquals(Math.pow(2, -1), MathUtils.power(2, -1), 0.0);
        assertEquals(Math.pow(2, -2), MathUtils.power(2, -2), 0.0);
    }

    @Test
    public void textXInPowerOfY() {
        assertEquals(Math.pow(2, 0), MathUtils.xInPowerOfY(2, 0), 0.0);
        assertEquals(Math.pow(2, 1), MathUtils.xInPowerOfY(2, 1), 0.0);
        assertEquals(Math.pow(2, 2), MathUtils.xInPowerOfY(2, 2), 0.0);
        assertEquals(Math.pow(2, 3), MathUtils.xInPowerOfY(2, 3), 0.0);
        assertEquals(Math.pow(2, 5), MathUtils.xInPowerOfY(2, 5), 0.0);
        assertEquals(Math.pow(2, -1), MathUtils.xInPowerOfY(2, -1), 0.0);
        assertEquals(Math.pow(2, -2), MathUtils.xInPowerOfY(2, -2), 0.0);
    }
}